# Using repository
Manualy append new repository to /etc/pacman.conf

```ini
[personal-repository]
SigLevel = Optional DatabaseOptional
Server = https://personal-archiso.gitlab.io/repository/x86_64
```
Or do it with a command
```bash
echo -e "
[personal-repository]
SigLevel = Optional DatabaseOptional
Server = https://personal-archiso.gitlab.io/repository/x86_64" \
| sudo tee -a /etc/pacman.conf
```

# Development

## Creating new empty repository
```bash
repo-add x86_64/personal-repository.db.tar.gz
```

## Adding package to repository
```bash
repo-add x86_64/personal-repository.db.tar.gz package-name
```

## Removing package from repository
```bash
repo-remove x86_64/personal-repository.db.tar.gz package-name
```
