#!/bin/bash

# Remove package from repository

cd $(dirname $0)

repo-remove ./x86_64/personal-repository.db.tar.gz $1
